# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def create_user
  user = User.new
  user.last_name , user.first_name, user.surname = g_user_names
  user.save!
  @user_id = user.id
end

def create_position
  g_position_name.each do |p_name|
    position = Position.new
    position.save!
    @name = p_name
    @true_position_id = position.id
    create_user
    create_employment_term
    new_user_position_history
    @true_position_id = nil
  end
end

def new_user_position_history
  PositionHistory.new_user = {position_id: @position_id, begin_date: @begin_date, name: @name}
end

def create_employment_term
  e_t = EmploymentTerm.new
  @position_id = @true_position_id || rand(1..6)
  e_t.position_id = @position_id
  e_t.user_id = @user_id
  e_t.division_id = rand(1..9)
  @begin_date = DateTime.now - rand(40).days
  e_t.begin_date = @begin_date
  e_t.save!
end

def g_user_names
  f_n = ['Петров', 'Иванов','Гончаров','Мерзляков','Овсяников','Гузачев']
  l_n = ['Иван', 'Алексей', 'Александр', 'Владимир', 'Илья', 'Олег']
  s_n = ['Иванов', 'Александрович', 'Олегович', 'Владимирович', 'Петрович', 'Анатольевич']
  [f_n[rand(0..5)],l_n[rand(0..5)],s_n[rand(0..5)]]
end

def g_position_name
  ['Программист', 'Саппорт','Аналитик','Тестировщик','Рекрутер','Проектный Менеджер']
end

def create_division
  [[1, 1], [2, 1],[3, 1],[4, 3],[5, 3],[6, 3],
    [7, 4],[8, 5], [9, 8]].each do |ids|
    division = Division.new
    division.id = ids.first
    division.name = ids.first.to_s
    division.parent_id = ids.last
    division.save!
  end
end

unless Rails.env.production?
  create_division
  create_position
  100.times do
    create_user
    create_employment_term
    new_user_position_history
  end
end