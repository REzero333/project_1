require "rails_helper"
RSpec.describe "TakeDataOnFront", type: :class do

before do
  User.delete_all
  EmploymentTerm.delete_all
  Position.delete_all
  Division.delete_all
  PositionHistory.delete_all
end

  context "Сгеренерирует дивы с таблицами в который пользователи и должности" do
    before do
      5.times{ create_employment_term }
      @first_date = DateTime.now - 1.days
      @last_date = DateTime.now + 1.days
    end

    it 'Все пользователи попадают в range дат' do
      result = service.to_s
      User.find_each do |user|
        full_name, pos_name, div_id = params(user)
        expect(result).to include(full_name)
        expect(result).to include(pos_name)
        expect(result).to include(div_id.to_s)
      end
    end

    it 'Никто не попдает в range дат (пустые блоки)' do
      @first_date = @first_date - 10.days
      @last_date = @last_date - 10.days
      result = service.to_s
      User.find_each do |user|
        full_name, pos_name, div_id = params(user)
        expect(result.include?(full_name)).to be_falsey
        expect(result.include?(pos_name)).to be_falsey
        expect(result.include?(div_id.to_s)).to be_truthy
      end
    end
  end

  def params(user)
    e_t = EmploymentTerm.find_by_user_id(user.id)
    full_name = "#{user.last_name} #{user.first_name} #{user.surname}"
    pos_name = e_t.position.name
    div_id = e_t.division_id
    [full_name, pos_name, div_id]
  end

  def service
    TakeDataOnFront.call(@first_date, @last_date)
  end

  def create_employment_term
    user = FactoryBot.create(:user)
    position = FactoryBot.create(:position)
    position_h = FactoryBot.create(:position_history, position_id: position.id)
    division = FactoryBot.create(:division)
    employment_term = FactoryBot.create(:employment_term,
                                          position_id: position.id,
                                          division_id: division.id,
                                          user_id: user.id)
    employment_term
  end
end