class ShowTableController < ApplicationController

	def index
		render partial: 'layouts/tables'
	end

	def index_user
		render partial: 'layouts/tables_user'
	end

	def take
		result = TakeDataOnFront.call(table_params[:first_date],
												 					table_params[:last_date])
		respond_to do |format|
    	format.json { render json: {"value" => result}}
  	end
	end

	def take_user
		result = TakeUserDataOnFront.call
		respond_to do |format|
    	format.json { render json: {"value" => result}}
  	end
	end

	def take_user_history
		result = TakeUserDataOnFront.history(table_params[:first_date],
												 							table_params[:last_date],
												 							table_params[:user_id])
		respond_to do |format|
    	format.json { render json: {"value" => result}}
  	end
	end

	def table_params
		params.permit(:first_date, :last_date, :division_id, :user_id)
	end
end