class EmploymentTermController < ApplicationController

	def create 
		old_e_t = EmploymentTerm.where( end_date: nil, user_id: employment_term_params[:user_id]).take
		e_t = EmploymentTerm.new
		e_t.position_id = employment_term_params[:position_id]
		e_t.division_id = employment_term_params[:division_id]
		e_t.user_id = employment_term_params[:user_id]
		begin_date = DateTime.now
		e_t.begin_date = begin_date
		old_e_t.close if old_e_t.present?
		e_t.save
		PositionHistory.new_user = {position_id: employment_term_params[:position_id], begin_date: begin_date, name: e_t.position.name}
		redirect_to '/'
	end

	def employment_term_params
		params.permit(:position_id, :division_id, :user_id, :id)
	end
end