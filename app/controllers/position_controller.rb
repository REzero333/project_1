class PositionController < ApplicationController

	def create
		pos = Position.new
		pos.create_name = position_params[:name]
		pos.save!
		redirect_to "/"
	end

	def update
		pos = Position.find(position_params[:id])
		pos.update_name = position_params[:name]
		pos.save!
		redirect_to "/"
	end

	def position_params
		params.permit(:name, :id)
	end
end