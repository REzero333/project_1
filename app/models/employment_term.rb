class EmploymentTerm < ActiveRecord::Base
  belongs_to :user, foreign_key: "user_id"
  belongs_to :division, foreign_key: "division_id"
  belongs_to :position, foreign_key: "position_id"

  def close
  	self.position.history(self.begin_date).close
  	self.update_column(:end_date,  DateTime.now)
  end
end
