class PositionHistory < ActiveRecord::Base
  belongs_to :position, foreign_key: "position_id"

  def close
  	self.update_column(:end_date, DateTime.now)
  end
  
  def self.new_user=(user_params)
    begin_date = user_params[:begin_date] || DateTime.now
    name = user_params[:name] || Position.find(user_params[:position_id]).name
  	PositionHistory.create!(begin_date: begin_date,
											  		position_id: user_params[:position_id],
											  		name: name)
  end
end