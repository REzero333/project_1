class TakeDataOnFront
	
	def initialize(first_date:, last_date:)
		@first_date = first_date
		@last_date = last_date
	end

	def self.call(first_date, last_date)
		serivce = TakeDataOnFront.new(first_date: first_date, last_date: last_date)
		serivce.fill_data
	end

	def fill_data
		@filled_data = {}
		create_division
		fill_users_and_positions
		build_html
	end

	def build_html
		@html = ''
		divisions.each do |division|
			sort_data = @filled_data[division.id].keys.sort
			data = []
			sort_data.map{ |user_name| data << [user_name, @filled_data[division.id][user_name]]}
			@html += "<div class='table' id='table_#{division.id}' style='display: none;'><table>"
			data.each do |d|
				@html += "<tr><td>#{d.first}</td><td>/</td><td>#{d.last}</td></tr>"
			end
			@html += "</table></div>"
		end
		@html
	end

	def fill_users_and_positions
		employment_terms.each do |e_t|
			user = users.detect{ |u| e_t.user_id == u.id}
			next if user.nil?
			@filled_data[e_t.division_id]["#{user.last_name} #{user.first_name} #{user.surname}"] = position.detect{ |pos| pos.id == e_t.position_id }.name					 
		end
	end

	def users
		@users ||= User.where(id: employment_terms.pluck(:user_id))
	end

	def employment_terms
		@employment_terms ||= EmploymentTerm.where(end_date: nil)
											.where("`employment_terms`.`begin_date` >= ? AND `employment_terms`.`begin_date` <= ?", @first_date , @last_date)
											.select(:user_id, :position_id, :division_id)
	end

	def position
		@position ||= Position.select("*")
	end

	def divisions
		@division ||= Division.select("*")
	end

	def create_division
		divisions.each do |d|
			@filled_data[d.id] = {}
		end
	end
end