Rails.application.routes.draw do
	get '/', to: 'application#show'
	post '/user_create', to: 'user#create'
	post '/user_update', to: 'user#update'
	post '/user_delete', to: 'user#delete'
	post '/employment_term_create', to: 'employment_term#create'
	post '/position_create', to: 'position#create'
	post '/position_update', to: 'position#update'

	get '/show', to: 'show_table#index'
	post '/take_table_data', to: 'show_table#take'
	get '/show_user', to: 'show_table#index_user'
	post '/take_user_table_data', to: 'show_table#take_user'
	post '/take_user_history', to: 'show_table#take_user_history'
end
