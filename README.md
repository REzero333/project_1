# README


Запуск тестовго проекта:

В терминале в корне проекта последовательно вводим команды

1) `docker-compose build`
2) `docker volume create --name=my_datavolume`
3) `docker-compose up -d`
4) `docker exec -ti project_1_web_1 bash`
5) `bin/rails db:create db:migrate db:seed`

URL: http://localhost:3000/ || http://127.0.0.1:3000

